### 前言
`ATOM`是`GitHub`专门为程序员推出的一个跨平台文本编辑器,是程序员非常喜爱的代码编辑器之一,`码云`也提供了`ATOM`插件扩展,但是很遗憾的是该插件作者已经不更新了,目前的版本是`1.7.2`
>参考：https://gitee.com/GitGroup/atom-gitee

本文（基于Windows 64位操作系统）只使用`Gitee`插件的实践过程做个简单使用教程

`ATOM`优点如下：
1. 小清新界面让人耳目一新.颜控必备

2. 丰富的插件几乎能够满足所有 web 开发需求

3. git 原生支持

4. 简单的插件编写

5. 自定义界面[如右图可以更改展示和背景,ui 等等]

---

### 一、 ATOM安装
1. 官网下载 [https://atom.io/](https://atom.io/)

2. GitHub下载 [https://github.com/atom/atom/releases/tag/v1.37.0](https://github.com/atom/atom/releases/tag/v1.37.0)

>注意：使用安装包会自动安装到C盘，有分类洁癖的同学可以下载zip包


### 二、Git安装

>本文不做详细解释了,参考：https://git-scm.com/


### 三、Gitee插件安装
1. 在命令行或者终端中运行:

  `apm install gitee`

2. 选择File 找到Setting目录中Install

![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/091920_63881edd_1976963.png "插件安装截图.png")

>安装完成无需重启,选择Packages 就看到码云了

![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/100114_1fec62c5_1976963.png "按钮1.png")


### 四、码云
      码云上创建一个demo项目
![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/131427_cd395136_1976963.png "atom.png")


### 四、Gitee插件使用
- #### 克隆项目

>点击码云菜单下的克隆项目,输入码云账号,选择项目拉取代码就可以啦   

![![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/110727_713a0d8f_1976963.png "输入账号.png")](https://images.gitee.com/uploads/images/2019/0531/110702_d15c87a0_1976963.png "登录.png")
![![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/111410_d51de967_1976963.png "拉取2.png")](https://images.gitee.com/uploads/images/2019/0531/111346_df578cc1_1976963.png "拉取.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/131634_dea24542_1976963.png "结果.png")

>>温馨提示：点击拉取代码出现`未安装Git客户端错误`，Git环境变量一定没配，在命令行中输入 `git --version` 能否出现版本信息.作者源码<br>
```
effective: (callback) ->
    git 'git --version'
    .then (data) ->
      callback(null)
      return
    .fail (err) ->
      callback(err)
```


- #### 创建项目

>创建项目会在码云上面创建一个，项目属性都是默认的-简洁明了

![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/135835_0f3af123_1976963.png "创建项目.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/135845_55e868bc_1976963.png "创建结果.png")

- #### 提交修改

>写下注释,提交成功后不会提示,提交失败会提示提交失败,但是不会提示具体错误，如果你在码云上先改了代码，再用插件提交，是提交不上去的

![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/133059_e3dfd627_1976963.png "提交代码.png")

- #### 创建分支、切换分支

>分支的创建和切换

![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/134304_00a7bb71_1976963.png "创建分支.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/134750_a7afe22d_1976963.png "切换分支.png")

- #### 查看修改

>查看已修改的,源码是调用 git --no-pager diff

![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/131912_6545eca2_1976963.png "查看修改.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/134750_a7afe22d_1976963.png "切换分支.png")

- #### 仓库、问题、历史

> 很遗憾这三个菜单都会出现如下问题，但是不影响，这三个应该都是只是跳`Gitee`的链接

  ![输入图片说明](https://images.gitee.com/uploads/images/2019/0531/132617_5edb7ab7_1976963.png "error.png")

---
>基本的流程都在这了，大家可以尝试下。
